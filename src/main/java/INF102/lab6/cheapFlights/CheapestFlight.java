package INF102.lab6.cheapFlights;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

import INF102.lab6.graph.DirectedEdge;
import INF102.lab6.graph.WeightedDirectedGraph;


public class CheapestFlight implements ICheapestFlight {


    @Override
    public WeightedDirectedGraph<City, Integer> constructGraph(List<Flight> flights) {
        List<City> cities = City.CITIES;
        
        WeightedDirectedGraph<City, Integer> graph = new WeightedDirectedGraph<>();

        for(City city:cities) {
            graph.addVertex(city);
        }
        
        for(Flight flight: flights) {
            graph.addEdge(flight.start, flight.destination, flight.cost);
        }
        return graph;
        
    }

    @Override
    public int findCheapestFlights(List<Flight> flights, City start, City destination, int nMaxStops) {
        
        WeightedDirectedGraph<City, Integer> graph = constructGraph(flights);
        Map<City, List<Pair>> adjMap = new HashMap<>();

        PriorityQueue<Pair> toSearch = new PriorityQueue<>(Comparator.comparingInt((Pair p)->p.cost));
        
        toSearch.add(new Pair(start, 0,0));

        while(!toSearch.isEmpty()) {
            Pair currPair = toSearch.poll();
            City currCity = currPair.city;
            int stops = currPair.stops, cost = currPair.cost;
            
            if(currCity == destination) {
                return cost;
            }
            if(stops > nMaxStops) {
                continue;
            }

            for(City city : graph.outNeighbours(currCity)) {
                int newCost = graph.getWeight(currCity, city);
                toSearch.add(new Pair(city, newCost + cost, stops + 1));
    
            }

        }

        throw new IllegalArgumentException("No flights possible");
        
    }



}

class Pair{
   City city;
   int stops;
   int cost;

    public Pair(City city, int cost, int stops) {
        this.city = city;
        this.stops = stops;
        this.cost = cost;
    }
}
